#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import boto3
import argparse
import os
import configparser


cfg = configparser.ConfigParser()
cfg.read('props.ini')


bucket_name = cfg['storage']['bucket_name']

session = boto3.session.Session(
        aws_access_key_id=cfg['aws']['aws_access_key_id'],
        aws_secret_access_key=cfg['aws']['aws_secret_access_key']
    )
s3 = session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net'
)

def upload_photos(path, album):
    s3.put_object(Bucket=bucket_name, Key=album + '/')
    for file in os.listdir(path):
        if file.endswith(".jpg") or file.endswith(".jpeg"):
            print(file)
            s3.upload_file(os.path.join(path, file), bucket_name, album + '/' + file)
    print('done')
    
def download_photos(path, album):
    check_album_exists(album)
    os.chdir(path)
    for photo in s3.list_objects(Bucket=bucket_name, Prefix=album + '/')['Contents']:
        filename = photo['Key'].split('/')[1]
        if filename != '':
            print(filename)
            with open(filename, 'wb') as file:
                s3.download_fileobj(Bucket=bucket_name, Key=photo['Key'], Fileobj=file)
    print('done')
        
def albums():
    files= s3.list_objects(Bucket=bucket_name)['Contents']
    for file in files:
        if file['Size'] == 0:
            print(file['Key'])
            
def album_photos(album):
    check_album_exists(album)
    for photo in s3.list_objects(Bucket=bucket_name, Prefix=album + '/')['Contents']:
        print(photo['Key'].split('/')[1])

def check_album_exists(album):
    files= s3.list_objects(Bucket=bucket_name)['Contents']
    for file in files:
        if file['Key'] == album + '/' and file['Size'] == 0:
            return
    raise Exception('Album ' + album + ' not found')
    
def main():
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(dest='command')
    
    upload = subparser.add_parser('upload')
    download = subparser.add_parser('download')
    list = subparser.add_parser('list')
    
    upload.add_argument('-p', type=str, required=True)
    upload.add_argument('-a', type=str, required=True)
    
    download.add_argument('-p', type=str, required=True)
    download.add_argument('-a', type=str, required=True)
    
    list.add_argument('-a', type=str, required=False)
   
    args = parser.parse_args()
    if args.command == 'upload':
        upload_photos(args.p, args.a)
    elif args.command == 'download':
        download_photos(args.p, args.a)
    elif args.command == 'list':
        if args.a is None:
            albums()
        else:
            album_photos(args.a)
    
main()