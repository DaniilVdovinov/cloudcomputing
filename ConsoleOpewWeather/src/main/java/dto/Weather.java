package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

/**
 * 08.09.2021
 *
 * @author Daniil Vdovinov
 */
@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
    public String main;
    public String description;
}
