import dto.AddressDto;
import dto.AddressWeather;

import java.util.Scanner;

/**
 * 08.09.2021
 *
 * @author Daniil Vdovinov
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DadataClient dadataClient = DadataClient.getInstance();
        OpenWeatherClient openWeatherClient = OpenWeatherClient.getInstance();

        System.out.println("Введите адрес (для выхода введите 0): ");
        String q = scanner.nextLine();
        while (!q.equals("0")) {
            try {
                AddressDto address = dadataClient.searchAddress(q);
                System.out.println("Адрес: " + address.getResult());

                AddressWeather weather = openWeatherClient.searchWeather(address);
                System.out.println("Температура: " + weather.getMain().getTemp());
                weather.getWeather().forEach(w -> System.out.println("Погода: " + w.getDescription()));

            } catch (Exception e) {
                System.out.println("Ошибка");
            }
            System.out.println("\nВведите адрес (для выхода введите 0): ");
            q = scanner.nextLine();
        }
    }
}
