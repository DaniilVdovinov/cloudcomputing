import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import dto.AddressDto;
import lombok.SneakyThrows;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.List;

/**
 * 08.09.2021
 *
 * @author Daniil Vdovinov
 */
public class DadataClient {
    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;

    public static final String BASE_URL = "https://cleaner.dadata.ru/api/v1";
    public static final String ADDRESS_URL = BASE_URL + "/clean/address";

    private DadataClient() {
        httpClient = HttpClient.newHttpClient();
        objectMapper = new JsonMapper();
    }

    public static DadataClient getInstance() {
        return new DadataClient();
    }

    @SneakyThrows
    public AddressDto searchAddress(String query) {
        List<String> strings = Collections.singletonList(query);
        String body = objectMapper.writeValueAsString(strings);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(ADDRESS_URL))
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", "Token baa86c06789d2d1016de46bbf0f34c39ac17936f")
                .setHeader("X-Secret", "a5a7b8279045950c78f29442fc0d1fddea5e3bad")
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        AddressDto[] addresses = objectMapper.readValue(response.body(), AddressDto[].class);
        if (addresses.length == 0) {
            throw new IllegalArgumentException("Address not foung");
        }
        return addresses[0];
    }
}
