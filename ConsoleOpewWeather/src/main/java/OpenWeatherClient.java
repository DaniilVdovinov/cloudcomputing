import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import dto.AddressDto;
import dto.AddressWeather;
import lombok.SneakyThrows;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * 08.09.2021
 *
 * @author Daniil Vdovinov
 */
public class OpenWeatherClient {
    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;

    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5";
    public static final String WEATHER_URL = BASE_URL + "/weather?lat=%f&lon=%f&appid=%s&lang=RU&units=metric";

    private OpenWeatherClient() {
        httpClient = HttpClient.newHttpClient();
        objectMapper = new JsonMapper();
    }

    public static OpenWeatherClient getInstance() {
        return new OpenWeatherClient();
    }

    @SneakyThrows
    public AddressWeather searchWeather(AddressDto address) {
        String url = String.format(WEATHER_URL, address.getGeoLat(), address.getGeoLon(), "10f55b11daa685e3cdea38700e428adc");
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return objectMapper.readValue(response.body(), AddressWeather.class);
    }
}
