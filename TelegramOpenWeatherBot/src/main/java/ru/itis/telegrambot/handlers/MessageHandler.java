package ru.itis.telegrambot.handlers;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendVoice;
import org.telegram.telegrambots.meta.api.objects.*;
import ru.itis.telegrambot.bot.MainBot;
import ru.itis.telegrambot.dto.AddressDto;
import ru.itis.telegrambot.dto.AddressWeather;
import ru.itis.telegrambot.services.LocationService;
import ru.itis.telegrambot.services.SpeechService;
import ru.itis.telegrambot.services.WeatherService;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;

@Component
@RequiredArgsConstructor
public class MessageHandler {

    private final LocationService locationService;
    private final WeatherService weatherService;
    private final SpeechService speechService;
    private final MainBot bot;

    public void handle(Update update) throws Exception {
        Message message = update.getMessage();
        String chatId = message.getChatId().toString();
        if (message.hasText()) {
            List<AddressDto> addresses = locationService.getAddresses(Collections.singletonList(message.getText()));
            bot.sendTextMessage(addresses.get(0).getResult(), chatId);

            if (addresses.size() > 0) {
                AddressWeather weather = weatherService.getAddressWeather(addresses.get(0));
                bot.sendTextMessage(weather.getWeather().get(0).getDescription() + ", " + weather.getMain().getTemp() + "C", chatId);
            }
        }
        if (message.hasVoice()) {
            String speech = speechService.recognizeSpeech(getBytes(message.getVoice()));
            bot.sendTextMessage(speech, chatId);

            List<AddressDto> addresses = locationService.getAddresses(Collections.singletonList(speech));
            bot.sendTextMessage(addresses.get(0).getResult(), chatId);

            if (addresses.size() > 0) {
                AddressWeather weather = weatherService.getAddressWeather(addresses.get(0));
                bot.sendTextMessage(weather.getWeather().get(0).getDescription() + ", " + weather.getMain().getTemp() + "C", chatId);
                String formattedWeather = weather.getWeather().get(0).getDescription() + ", температура " + weather.getMain().getTemp() + " градуса";
                byte[] bytes = speechService.synthesizeSpeech(formattedWeather);
                bot.execute(
                        SendVoice.builder()
                                .voice(new InputFile(new ByteArrayInputStream(bytes), "file.ogg"))
                                .chatId(chatId)
                                .build()
                );
            }

        }

    }

    private byte[] getBytes(Voice voice) throws Exception {
        GetFile getFile = new GetFile();
        getFile.setFileId(voice.getFileId());
        File file = bot.execute(getFile);
        String fileUrl = File.getFileUrl(bot.getBotToken(), file.getFilePath());
        InputStream inputStream = new URL(fileUrl).openStream();
        return inputStream.readAllBytes();
    }


}
