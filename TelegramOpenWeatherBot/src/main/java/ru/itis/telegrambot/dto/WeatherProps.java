package ru.itis.telegrambot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

/**
 * 08.09.2021
 *
 * @author Daniil Vdovinov
 */
@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherProps {
    public double temp;
    public double feels_like;
    public double temp_min;
    public double temp_max;
    public int pressure;
    public int humidity;
}
