package ru.itis.telegrambot.dto;

import lombok.Data;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@Data
public class OAuthTokenDto {
    private String yandexPassportOauthToken;

    public OAuthTokenDto(String OAuthToken) {
        this.yandexPassportOauthToken = OAuthToken;
    }

}
