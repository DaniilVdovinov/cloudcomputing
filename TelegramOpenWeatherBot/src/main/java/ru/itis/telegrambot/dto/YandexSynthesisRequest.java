package ru.itis.telegrambot.dto;

import lombok.Builder;
import lombok.Data;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@Data
@Builder
public class YandexSynthesisRequest {
    private String text;
    private String folderId;
}
