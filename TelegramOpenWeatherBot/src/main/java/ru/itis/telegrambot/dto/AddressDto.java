package ru.itis.telegrambot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 08.09.2021
 *
 * @author Daniil Vdovinov
 */
@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressDto {
    private String source;
    private String result;
    private Integer qc;
    @JsonProperty("geo_lat")
    private Double geoLat;
    @JsonProperty("geo_lon")
    private Double geoLon;
}
