package ru.itis.telegrambot.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * 08.09.2021
 *
 * @author Daniil Vdovinov
 */
@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressWeather {
    private List<Weather> weather;
    private WeatherProps main;
}
