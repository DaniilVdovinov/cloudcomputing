package ru.itis.telegrambot.bot;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.itis.telegrambot.configurations.BotConfiguration;
import ru.itis.telegrambot.handlers.MessageHandler;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@Component
@Slf4j
public class MainBot extends TelegramLongPollingBot {

    @Autowired
    private MessageHandler messageHandler;
    @Autowired
    private BotConfiguration botConfiguration;

    @Override
    public String getBotUsername() {
        return botConfiguration.getName();
    }

    @Override
    public String getBotToken() {
        return botConfiguration.getToken();
    }

    @SneakyThrows
    public void sendTextMessage(String text, String chatId) {
        SendMessage message = SendMessage.builder()
                .text(text)
                .chatId(chatId)
                .build();
        execute(message);
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            messageHandler.handle(update);
        } catch (Exception e) {
            log.info("error", e);
        }
    }
}
