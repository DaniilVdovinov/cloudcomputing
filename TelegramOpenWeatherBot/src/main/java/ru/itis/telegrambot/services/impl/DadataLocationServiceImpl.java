package ru.itis.telegrambot.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.telegrambot.clients.DadataClient;
import ru.itis.telegrambot.dto.AddressDto;
import ru.itis.telegrambot.services.LocationService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@Service
@RequiredArgsConstructor
public class DadataLocationServiceImpl implements LocationService {

    private final DadataClient client;

    @Override
    public List<AddressDto> getAddresses(List<String> queries) {
        return client.getAddressByQuery(queries, getHeaderMap());
    }

    private Map<String, String> getHeaderMap() {
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", "Token baa86c06789d2d1016de46bbf0f34c39ac17936f");
        map.put("X-Secret", "a5a7b8279045950c78f29442fc0d1fddea5e3bad");
        return map;
    }

}
