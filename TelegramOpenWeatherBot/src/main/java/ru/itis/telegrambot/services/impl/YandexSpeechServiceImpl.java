package ru.itis.telegrambot.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.telegrambot.clients.YandexSpeechRecognizeClient;
import ru.itis.telegrambot.clients.YandexSpeechSynthesizeClient;
import ru.itis.telegrambot.configurations.YandexTokenConfig;
import ru.itis.telegrambot.dto.Result;
import ru.itis.telegrambot.dto.YandexSynthesisRequest;
import ru.itis.telegrambot.services.SpeechService;

import java.util.HashMap;
import java.util.Map;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@Service
@RequiredArgsConstructor
public class YandexSpeechServiceImpl implements SpeechService {

    private final YandexSpeechRecognizeClient recognizeClient;
    private final YandexSpeechSynthesizeClient synthesizeClient;
    private final YandexTokenConfig tokenConfig;

    @Override
    public String recognizeSpeech(byte[] bytes) {
        Result result = recognizeClient.recognizeAudio(getAuthHeader(), getQueries(), bytes);
        return result.getResult();
    }

    @Override
    public byte[] synthesizeSpeech(String text) {
        YandexSynthesisRequest request = YandexSynthesisRequest.builder()
                .folderId("b1gbuf6n1els1qarlvim")
                .text(text)
                .build();
        return synthesizeClient.synthesizeSpeech(request, getAuthHeader());
    }

    private Map<String, String> getQueries() {
        HashMap<String, String> map = new HashMap<>();
        map.put("folderId", "b1gbuf6n1els1qarlvim");
        return map;
    }

    private String getAuthHeader() {
        return "Bearer " + tokenConfig.getIAmToken();
    }

}
