package ru.itis.telegrambot.services;

import ru.itis.telegrambot.dto.AddressDto;
import ru.itis.telegrambot.dto.AddressWeather;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
public interface WeatherService {
    AddressWeather getAddressWeather(AddressDto address);
}
