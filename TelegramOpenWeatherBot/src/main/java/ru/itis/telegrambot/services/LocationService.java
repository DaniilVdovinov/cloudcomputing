package ru.itis.telegrambot.services;

import ru.itis.telegrambot.dto.AddressDto;

import java.util.List;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
public interface LocationService {
    List<AddressDto> getAddresses(List<String> queries);
}
