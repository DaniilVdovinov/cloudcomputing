package ru.itis.telegrambot.services;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
public interface SpeechService {
    String recognizeSpeech(byte[] bytes);
    byte[] synthesizeSpeech(String text);
}
