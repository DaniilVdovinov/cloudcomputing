package ru.itis.telegrambot.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.telegrambot.clients.OpenWeatherClient;
import ru.itis.telegrambot.dto.AddressDto;
import ru.itis.telegrambot.dto.AddressWeather;
import ru.itis.telegrambot.services.WeatherService;

import java.util.HashMap;
import java.util.Map;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@Service
@RequiredArgsConstructor
public class OpenWeatherServiceImpl implements WeatherService {

    private final OpenWeatherClient client;

    @Override
    public AddressWeather getAddressWeather(AddressDto address) {
        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("lat", address.getGeoLat().toString());
        queryMap.put("lon", address.getGeoLon().toString());

        queryMap.put("units", "metric");
        queryMap.put("lang", "RU");
        queryMap.put("appid", "10f55b11daa685e3cdea38700e428adc");

        return client.getAddressWeather(queryMap);
    }
}
