package ru.itis.telegrambot.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import ru.itis.telegrambot.configurations.FeignConfig;
import ru.itis.telegrambot.dto.AddressWeather;

import java.util.Map;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@FeignClient(name = "openWeatherClient", url = "https://api.openweathermap.org/data/2.5", configuration = FeignConfig.class)
public interface OpenWeatherClient {

    @GetMapping("/weather")
    AddressWeather getAddressWeather(@SpringQueryMap Map<String, String> queryMap);
}
