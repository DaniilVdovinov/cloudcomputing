package ru.itis.telegrambot.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.itis.telegrambot.dto.Result;

import java.util.Map;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@FeignClient(name = "yandexSpeechClient", url = "https://stt.api.cloud.yandex.net/speech/v1")
public interface YandexSpeechRecognizeClient {
    @PostMapping("/stt:recognize")
    Result recognizeAudio(@RequestHeader("Authorization") String token, @SpringQueryMap Map<String, String> queryMap, @RequestBody byte[] bytes);


}
