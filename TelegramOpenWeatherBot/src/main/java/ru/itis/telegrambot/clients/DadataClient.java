package ru.itis.telegrambot.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.itis.telegrambot.dto.AddressDto;

import java.util.List;
import java.util.Map;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@FeignClient(name = "dadataClient", url = "https://cleaner.dadata.ru/api/v1")
public interface DadataClient {

    @PostMapping("/clean/address")
    List<AddressDto> getAddressByQuery(@RequestBody List<String> queries, @RequestHeader Map<String, String> headers);
}
