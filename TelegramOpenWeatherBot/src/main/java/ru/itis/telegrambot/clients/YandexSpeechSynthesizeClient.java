package ru.itis.telegrambot.clients;

import feign.Headers;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.itis.telegrambot.configurations.FormFeignConfig;
import ru.itis.telegrambot.dto.YandexSynthesisRequest;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@FeignClient(name = "yandesSynthesizeClient", url = "https://tts.api.cloud.yandex.net/speech/v1", configuration = FormFeignConfig.class)
public interface YandexSpeechSynthesizeClient {

    @PostMapping(value = "/tts:synthesize", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    byte[] synthesizeSpeech(YandexSynthesisRequest request, @RequestHeader("Authorization") String token);

}
