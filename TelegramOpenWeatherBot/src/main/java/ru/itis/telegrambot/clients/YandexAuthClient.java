package ru.itis.telegrambot.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.itis.telegrambot.dto.IAmTokenDto;
import ru.itis.telegrambot.dto.OAuthTokenDto;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@FeignClient(name = "yandexAuthClient", url = "https://iam.api.cloud.yandex.net/iam/v1/tokens")
public interface YandexAuthClient {
    @PostMapping
    IAmTokenDto execute(@RequestBody OAuthTokenDto token);

}
