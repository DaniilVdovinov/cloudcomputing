package ru.itis.telegrambot.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ru.itis.telegrambot.clients.YandexAuthClient;
import ru.itis.telegrambot.dto.IAmTokenDto;
import ru.itis.telegrambot.dto.OAuthTokenDto;

import javax.annotation.PostConstruct;

/**
 * 13.09.2021
 *
 * @author Daniil Vdovinov
 */
@Configuration
@EnableScheduling
public class YandexTokenConfig {
    @Autowired
    private YandexAuthClient client;

    public String getIAmToken() {
        return iAmToken;
    }

    private String iAmToken;

    @Scheduled(cron = "0 * */2 * * ?")
    public void updateToken() {
        IAmTokenDto response = client.execute(new OAuthTokenDto("AQAAAAAEYSmgAATuwYcobmX9Qk6MiVeBj_sNq90"));
        iAmToken = response.getIamToken();
    }

    @PostConstruct
    public void init() {
        updateToken();
    }
}
