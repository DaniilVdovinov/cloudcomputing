import requests
import json
from PIL import Image
from io import BytesIO
import boto3
import os
import base64
import time

key_id = os.environ['aws_access_key_id']
secret_key = os.environ['aws_secret_access_key']
i_am_token = os.environ['i_am_token']
queue_url = os.environ['queue_url']

face_url = 'https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze'

session = boto3.Session(
    aws_access_key_id=key_id,
    aws_secret_access_key=secret_key,
)
client = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
)

queue_client = session.client(
    service_name='sqs',
    endpoint_url='https://message-queue.api.cloud.yandex.net',
    region_name='ru-central1'
)

def encode_file(file):
    file_content = file.read()
    base64_bytes = base64.b64encode(file_content)
    return base64_bytes.decode('ascii') 

def send_faces_to_queue(faces):
    queue_client.send_message(
        QueueUrl=queue_url,
        MessageBody=json.dumps({
            'type' : 'faces',
            'data' : json.dumps(faces)
        })
    )


def handler(event, context):
    message =  event['messages'][0]
    details = message.get('details')

    if '/' in details['object_id']:
        return {'statusCode': 400}

    ext = os.path.splitext(details['object_id'])[1][1:]
    img_format = ext if ext != 'jpg' else 'jpeg'

    if ext not in ['jpeg', 'png', 'jpg']:
        return {'statusCode': 400}

    f = client.get_object(Bucket=details['bucket_id'], Key=details['object_id']).get('Body')

    file = bytearray()
    file[:] = f.read() 

    enfile = encode_file(BytesIO(file))

    headers = {
        'Authorization': 'Bearer ' + context.token.get('access_token')
    }
    body = {
        "folderId": message.get('event_metadata')['folder_id'],
        "analyze_specs": [
            {
                "content": enfile,
                "features": [{"type": "FACE_DETECTION"}]
                }
            ]
        }
    response = requests.post(face_url, headers=headers, json=body).json()
    results = response['results']
    
    res = results[0]['results'][0]['faceDetection']
    if bool(res):
        face_obj_ids = []

        for face in res['faces']:
            coords = face['boundingBox']['vertices']
            img = Image.open(BytesIO(file))
            left = int(coords[0].get('x'))
            top = int(coords[0].get('y'))
            right = int(coords[2].get('x'))
            bottom = int(coords[2].get('y'))
            img_crop = img.crop((left, top, right, bottom))

            buf = BytesIO()
            img_crop.save(buf, format=img_format)

            face_obj_id = details['object_id'] + '/' + str(time.time_ns() // 1_000_000) + '.' + ext

            client.upload_fileobj(BytesIO(buf.getvalue()), details['bucket_id'], face_obj_id)
            face_obj_ids.append(face_obj_id)

        if len(face_obj_ids) > 0:
            send_faces_to_queue(face_obj_ids)

    return {
        'statusCode': 200,
    }