# Настройка облачной функции
- Загрузить файлы index.py и requirements.txt в облачную функцию
- В переменных окружения облачной функции задать параметры
`aws_access_key_id`
`aws_secret_access_key`
`queue_url` - ссылка на очередь для отправки сообщений
- В параметрах функции указать сервисный аккаунт

# Настройка триггера
- В выбранном сервисном аккаунте создать триггер
- Указать тип - `Object Storage`
- Выбрать необходимый бакет
- Указать тип событий - `Создание объекта`

# Настройка очереди
- Создать очередь
- Сохранить ссылку на очередь