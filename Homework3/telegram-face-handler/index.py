import os
import requests as r
import json
import boto3
from io import BytesIO

bot_token = os.environ['bot_token']
chat = os.environ['chat_id']
url = "https://api.telegram.org/bot{token}/".format(token=bot_token)
server_url = os.environ['func_url']
person_folder_prefix = 'persons/'

key_id = os.environ['aws_access_key_id']
secret_key = os.environ['aws_secret_access_key']
bucket_id = os.environ['bucket_id']
session = boto3.Session(
    aws_access_key_id=key_id,
    aws_secret_access_key=secret_key,
)
client = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
)

def send_message(text):
    params = {'chat_id' : chat, 'text' : text}
    return r.post(url=url + 'sendMessage', data=params)

def send_photo(object_id):
    f = get_object_file(object_id)
    params = {'chat_id': chat}
    file_ = {'photo': f}
    return r.post(url=url + 'sendPhoto', params=params, files=file_)

def set_webhook():
    params = {'url' : server_url}
    resp = r.post(url=url + 'setWebhook', data=params)
    print(resp.json())
set_webhook()

def get_file(object_id):
    f = client.get_object(Bucket=bucket_id, Key=object_id).get('Body')
    file = bytearray()
    file[:] = f.read() 
    return BytesIO(file)

def get_object_file(object_id):
    f = client.get_object(Bucket=bucket_id, Key=object_id).get('Body')
    return f.read()

def upload_person_photo(object_id, who):
    person_photo_id = person_folder_prefix + who + '/' + object_id
    client.upload_fileobj(get_file(object_id), bucket_id, person_photo_id)

def find_and_send_person_faces(who):
    list_objects = client.list_objects(Bucket=bucket_id, Prefix=person_folder_prefix + who)
    if 'Contents' not in list_objects:
        send_message('Изображения не найдены')
    else:   
        for obj in list_objects['Contents']:
            send_photo(obj['Key'])

def find_and_send_person_photos(who):
    list_objects = client.list_objects(Bucket=bucket_id, Prefix=person_folder_prefix + who)
    if 'Contents' not in list_objects:
        send_message('Изображения не найдены')
    else:
        photos = set()
        for obj in list_objects['Contents']:
            paths = obj['Key'].split('/')
            key = paths[len(paths) - 2]
            photos.add(key)
        for photo in photos:
            send_photo(photo)


def handler(event, context):
    body = json.loads(event['body'])
    if 'message' in body:
        message = body['message']
        if 'reply_to_message' in message:
            reply_msg = message['reply_to_message']
            if 'photo' in reply_msg and 'caption' in reply_msg:
                text = reply_msg['caption']
                object_id = text.split('|')[0]
                who = message['text']
                upload_person_photo(object_id, who.lower())
        if message['text'].startswith('/faces'):
            who = message['text'][7:].lower()
            find_and_send_person_faces(who)
        if message['text'].startswith('/find'):
            who = message['text'][6:].lower()
            find_and_send_person_photos(who)

    return {
        'statusCode': 200,
        'body': 'ok',
    }
