# Схема работы
![Alt text](http://plantuml.com/plantuml/png/bP31JiCm44Jl_eezjXAeeiSSK4-82qAey05RP-e6OZlOcoB-dYR42aj1WgDtt-pCU9E5a_I-DQyzUj0TScQ7X_71MRrDpS8xpg3xtJEiR2KcTWEc1rUcuoIDRJeEGZTiiKMeaTJu4IrSOc_OIapqb97eJ7VBeMwH_cgyDaRblxp-ubxHdYqoBIGrpftSS4IGfPc9WTT5g287cOJT8Keq6j0ML2bKNow5DJFqrjk4hdqVcGafg-8rVKeX_MTZMhnXTuZnPMbcwBpIkgBCHr3ZsI6R7_njzFuafiLgtuJwWbPZtENlCOhWtq66-oM8tdttADGyqw7BVeDGzxxz0000?raw=true "Diagram")

# Настройка облачнхы функций. Для каждой функции:
- Загрузить файлы index.py и requirements.txt в облачную функцию
- В переменных окружения облачной функции задать параметры
`aws_access_key_id`,
`aws_secret_access_key`,
`bucket_id` - id бакета, где расположены изображения,
`func_url` - ссылка на облачную функцию (только для функции telegram-face-handler),
`bot_token` - токен бота,
`chat_id` - id чата, куда бот будет отсылать информацию
- В параметрах функции указать сервисный аккаунт
- Сделать функцию `telegram-face-handler` публичной  

# Настройка триггера
- В выбранном сервисном аккаунте создать триггер
- Указать тип - `Message Queue`
- Выбрать необходимую очередь
- Выбрать функцию - `telegram-face-sender`

# Команды боту
- `/find {name}` - найти фотографии с выбранным человеком
- `/faces {name}` - найти фотографии лиц выбранного челоевка
