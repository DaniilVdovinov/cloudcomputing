import os
import requests as r
import json
import boto3
import os

bot_token = os.environ['bot_token']
chat = os.environ['chat_id']
url = "https://api.telegram.org/bot{token}/".format(token=bot_token)

key_id = os.environ['aws_access_key_id']
secret_key = os.environ['aws_secret_access_key']
bucket_id = os.environ['bucket_id']
session = boto3.Session(
    aws_access_key_id=key_id,
    aws_secret_access_key=secret_key,
)
client = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
)

def get_object_file(object_id):
    f = client.get_object(Bucket=bucket_id, Key=object_id).get('Body')
    return f.read()

def send_message(text):
    params = {'chat_id' : chat, 'text' : text}
    return r.post(url=url + 'sendMessage', data=params)

def send_photo(object_id):
    f = get_object_file(object_id)
    params = {'chat_id': chat, 'caption': object_id + '|\nКто это ? (ответь на это сообщение)'}
    file_ = {'photo': f}
    return r.post(url=url + 'sendPhoto', params=params, files=file_)
    

def handler(event, context):
    for message in event['messages']:
        msg_body = json.loads(message['details']['message']['body'])
        print(msg_body)
        if msg_body['type'] == 'keys':
            for face_id in msg_body['data']:
                print(face_id)
                send_photo(face_id)
    
    return {
        'statusCode': 200,
        'body': message,
    }
